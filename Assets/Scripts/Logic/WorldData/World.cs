﻿using UnityEngine;
using System.Collections;

public class World : MonoBehaviour {


	static TDTileMap map;

	public int size_x = 50;
	public int size_z = 50;
	public int rooms = 5;
	private bool isBG = false;
	public string title = "My World";

	// Use this for initialization
	void Start () {
		map = new TDTileMap(size_x, size_z, isBG);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public TDTileMap getMap() {
		return map;
	}
}
