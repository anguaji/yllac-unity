﻿public class TDTile {

	public const int TILE_EMPTY = 0;
	public const int TILE_WALL_1 = 1;
	public const int TILE_WATER = 2;
	public const int TILE_WALL_2 = 3;
	
	public int type = TILE_EMPTY;

	public int getTileType() {
		return this.type;
	}

}