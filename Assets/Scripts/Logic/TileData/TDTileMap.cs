﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TDTileMap {

	protected class TDRoom {
		public int left;
		public int top;
		public int width;
		public int height;
		public bool isConnected = false;

		public int right {
			get {return left + width - 1;}
		}

		public int bottom {
			get {return top + height - 1; }
		}

		public int center_x {
			get { return left + width / 2; }
		}

		public int center_y {
			get { return top + height / 2; }
		}

		public bool CollidesWith(TDRoom r) {
			if(left > r.right - 1) 
				return false;

			if(top > r.bottom - 1) 
				return false;

			if(right < r.left - 2) 
				return false;

			if(bottom < r.top + 2) 
				return false;

			return true;
		}



	}

	int[,] _map_data;

	List<TDRoom> rooms;

	private int _height = 10;
	private int _width = 10;

	private int _chanceToBeWall = 2;
	private bool _isBG = false;

	private int numRooms = 15;

	/*
	 *
	 *	0 = sand
	 *	1 = tree
	 *	2 = water
	 *	3 = boulder
	 *	4 = floor
	 *	5 = wall
	 */

	public TDTileMap(int width, int height, bool setBG) {
		_height = height;
		_width = width;
		_isBG = setBG;
		_map_data = new int[_width, _height];

		rooms = new List<TDRoom>();

		generateMap();
		generateRooms();
		generateWalls();
	}

	private void generateMap() {

		for(int x = 0; x < _width; x++) {
			for(int y = 0; y < _height; y++) {
				if(_isBG) {
					_map_data[x, y] = 0;
				} else {
					if(Random.Range(1, 10) < _chanceToBeWall) {
						if(Random.Range(1, 10) > 5)
							_map_data[x, y] = 1;
						else 
							_map_data[x, y] = 3;
					} else {
						_map_data[x, y] = 0;
					}
				}
			}
		}
	}

	void generateRooms() {
		for(int i = 0; i < numRooms; i++) {
			var w = Random.Range(4, 20);
			var h = Random.Range(4, 20);
			var x = Random.Range(2, (_width - w - 2));
			var y = Random.Range(2, (_height - h - 2));
			
			TDRoom r = new TDRoom();
			r.left = x;
			r.top = y;
			r.width = w;
			r.height = h;
			
			if(!roomCollides(r)) {
				rooms.Add(r);
			}			
		}

		foreach(TDRoom r in rooms) {
			makeRoom(r);
		}

		//debug.Log("There are " + rooms.Count + " rooms");

		for(int i = 0; i < rooms.Count - 1; i++) {
			if(rooms[i].isConnected != true) {
				makeCorridor(rooms[i], rooms[i + 1]);
				rooms[i].isConnected = true;
				rooms[i + 1].isConnected = true;
			}
		}
	}

	void generateWalls() {
		for(int x = 0; x < _width; x++) {
			for(int y = 0; y < _height; y++) {
				if(_map_data[x, y] != 5 && hasAdjacentFloor(x, y)) {
					_map_data[x, y] = 4;
				}
			}
		}
	}

	bool hasAdjacentFloor(int x, int y) {
		if(x > 0 && _map_data[x - 1, y] == 5) 
			return true;

		if(x < _width - 1 && _map_data[x + 1, y] == 5) 
			return true;

		if(y > 0 && _map_data[x, y - 1] == 5) 
			return true;
		
		if(y < _height - 1 && _map_data[x, y + 1] == 5) 
			return true;

		return false;

	}

	bool roomCollides(TDRoom r) {
		foreach(TDRoom r2 in rooms) {
			if(r.CollidesWith(r2)) 
				return true;
		}
		return false;
	}

	public int GetTile(int x, int y) {
		if(x < 0 || x >= _width || y < 0 || y >= _height) {
			return 99;
		}
		return _map_data[x, y];
	}

	void makeRoom(TDRoom r) {

		for(int x = -1; x < r.width + 1; x++) {
			for(int y = -1; y < r.height + 1; y++){
				//_map_data[x + r.left, y + r.top] = 0;
			}
		}

		for(int x = 0; x < r.width; x++) {
			for(int y = 0; y < r.height; y++){
				if(x == 0 || x == r.width - 1 || y == 0 || y == r.height - 1) {
					_map_data[x + r.left, y + r.top] = 4;
				} else {
					_map_data[x + r.left, y + r.top] = 5;
				}

			}
		}
	}

	void makeCorridor(TDRoom r1, TDRoom r2) {
		int x = r1.center_x;
		int y = r1.center_y;

		while(x != r2.center_x) {
			_map_data[x, y] = 5;
			x += x < r2.center_x ? 1 : -1;
		}

		while(y != r2.center_y) {
			_map_data[x, y] = 5;
			y += y < r2.center_y ? 1 : -1;
		}
	}




}
