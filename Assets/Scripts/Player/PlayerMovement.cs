﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	
	public float keyThreshold = 0.13f;
	private float[] keyMovement = new float[4];
	private float keyMovementAll;
	
	// Use this for initialization
	void Start () {
		for(int i = 0; i < keyMovement.Length; i++) {
			keyMovement[i] = keyThreshold;
		}

		keyMovementAll = keyThreshold;
	}
	
	// Update is called once per frame
	void Update () {
		var newPos = new Vector3 ();
		if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.UpArrow)) {
			keyMovementAll += Time.deltaTime;
			if(keyMovementAll > keyThreshold) {
				newPos.z = 1;
				keyMovementAll = 0.0f;
			}
		}
		if (Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.DownArrow)) {
			keyMovementAll += Time.deltaTime;
			if(keyMovementAll > keyThreshold) {
				newPos.z = -1;
				keyMovementAll = 0.0f;
			}
		}
		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow)) {
			keyMovementAll += Time.deltaTime;
			if(keyMovementAll > keyThreshold) {
				newPos.x = -1;
				keyMovementAll = 0.0f;
			}
		}

		if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)) {
			keyMovementAll += Time.deltaTime;
			if(keyMovementAll > keyThreshold) {
				newPos.x = 1;
				keyMovementAll = 0.0f;
			}
		}

		if(Input.GetKeyUp (KeyCode.W) || Input.GetKeyUp (KeyCode.S) || Input.GetKeyUp (KeyCode.A) || Input.GetKeyUp (KeyCode.D)) {
			keyMovement[0] = keyThreshold;
			keyMovement[1] = keyThreshold;
			keyMovement[2] = keyThreshold;
			keyMovement[3] = keyThreshold;
			keyMovementAll = keyThreshold;
		}

		transform.position += newPos;
	}
}