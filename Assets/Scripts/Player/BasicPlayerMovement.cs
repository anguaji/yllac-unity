﻿using UnityEngine;
using System.Collections;

public class BasicPlayerMovement : MonoBehaviour {

	private float inputTimer;
	// Use this for initialization
	void Start () {
		//Debug.Log("Entered player movement script");
	}
	
	// Update is called once per frame
	void Update () {

		var position = new Vector3 (0, 0, 0);

		if (Input.GetKeyDown (KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {
			position.y = 1;
		}
		
		if (Input.GetKeyDown (KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) {
			position.y = -1;
		}
		
		if (Input.GetKeyDown (KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) {
			position.x = -1;
		}
		
		if (Input.GetKeyDown (KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) {
			position.x = 1;
		}

		transform.position += position;
	}

	void FixedUpdate() {
		
	}

}
